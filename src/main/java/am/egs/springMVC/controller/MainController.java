package am.egs.springMVC.controller;

import am.egs.springMVC.model.User;
import am.egs.springMVC.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class MainController {



    @Autowired
    UserService userService;


    @GetMapping(value = "/")
    public String main() {
        return "index";
    }

    @PostMapping(value = "/login")
    public String login(@RequestParam("email") String email, @RequestParam("password") String password) {
        User byEmail = userService.getByEmail(email);
        if (byEmail.getPassword().equals(password)) {
            return "redirect:/user";
        } else {
            return "index";
        }

    }

    @GetMapping(value = "/register")
    public String register() {
        return "register";
    }

    @PostMapping("/register")
    public String register(@ModelAttribute("user") User user, RedirectAttributes redirectAttributes) {
        if (userService.getByEmail(user.getEmail()) == null) {
            userService.add(user);
            return "index";
        } else {
            redirectAttributes.addAttribute("email", user.getEmail());
            return "redirect:/error";
        }
    }


    @GetMapping("/user")
    public String user(ModelMap map) {
        map.addAttribute("users", userService.get());
        return "home";
    }

    @GetMapping("/error")
    public String error(@RequestParam("email") String email, ModelMap map) {
        map.addAttribute("email", email);
        return "error";
    }
}
