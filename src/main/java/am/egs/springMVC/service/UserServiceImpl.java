package am.egs.springMVC.service;

import am.egs.springMVC.dao.UserDao;
import am.egs.springMVC.dao.UserDaoImpl;
import am.egs.springMVC.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;


    @Transactional
    public void add(User user) {
        userDao.add(user);
    }

    @Transactional
    public void delete(User user) {
        userDao.delete(user);
    }

    @Transactional
    public void deleteById(int id) {
        userDao.deleteById(id);
    }

    @Transactional(readOnly = true)
    public List<User> get() {
        return userDao.get();
    }

    @Transactional
    public User getById(int id) {
        return userDao.getById(id);
    }


    @Transactional
    public User getByEmail(String email) {
      return   userDao.getByEmail(email);
    }
}
