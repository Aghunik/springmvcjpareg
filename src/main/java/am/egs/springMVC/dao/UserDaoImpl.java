package am.egs.springMVC.dao;

import am.egs.springMVC.model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class UserDaoImpl implements UserDao {

    @PersistenceContext
    private EntityManager em;


    @Override
    public User getById(int id) {
        return em.find(User.class, id);
    }

    @Override
    public User getByEmail(String email) {
        List<User> users = em.createQuery("select u from User u where u.email = :email").setParameter("email", email).getResultList();
        User user = new User();
        if (users.size() < 1) {
            user.setEmail(null);
            return user;
        }
        return users.get(0);
    }


    @Override
    public void add(User user) {
        em.persist(user);
    }

    @Override
    public void delete(User user) {
        if (em.contains(user)) {
            em.remove(user);
        }
    }

    @Override
    public void deleteById(int id) {
        User byId = getById(id);
        if (em.contains(byId)) {
            em.remove(byId);
        }
    }

    @Override
    public List<User> get() {
        CriteriaQuery<User> criteriaQuery = em.getCriteriaBuilder().createQuery(User.class);
        @SuppressWarnings("unused")
        Root<User> root = criteriaQuery.from(User.class);
        return em.createQuery(criteriaQuery).getResultList();
    }
}
