package am.egs.springMVC.dao;

import am.egs.springMVC.model.User;

import java.util.List;

public interface UserDao {
    User getById(int id);

    User getByEmail(String email);

    void add(User user);

    void delete(User user);

    void deleteById(int id);

    List<User> get();
}
